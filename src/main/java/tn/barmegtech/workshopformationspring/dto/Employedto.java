package tn.barmegtech.workshopformationspring.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tn.barmegtech.workshopformationspring.entites.Employee;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employedto {
	private Long id;
	@Size(min = 3, max = 20)
	@NotBlank
	private String fullname;
	private Long age;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateRecrutement;
	
	public static Employee toentity(Employedto emp )
	{
		return Employee.builder()
		.fullname(emp.getFullname())
		.age(emp.getAge())
		.dateRecrutement(emp.getDateRecrutement())
		.build();
		
	}	
	public static Employedto fromentity(Employee emp )
	{
		return Employedto.builder()
			.id(emp.getId())	
		.fullname(emp.getFullname())
		.age(emp.getAge())
		.dateRecrutement(emp.getDateRecrutement())
		.build();
		
		
		
	}	
	}
	

