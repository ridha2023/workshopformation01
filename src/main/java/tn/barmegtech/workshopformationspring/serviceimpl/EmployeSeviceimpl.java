package tn.barmegtech.workshopformationspring.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;



import lombok.RequiredArgsConstructor;
import tn.barmegtech.workshopformationspring.dto.Employedto;
import tn.barmegtech.workshopformationspring.entites.Employee;
import tn.barmegtech.workshopformationspring.repository.EmployeRepository;
import tn.barmegtech.workshopformationspring.servics.EmployeSevice;
@Service
@RequiredArgsConstructor
public class EmployeSeviceimpl implements EmployeSevice{
	private final EmployeRepository employeRepository;

	@Override
	public Employedto ajouter(Employedto empdto) {
		Employee emp=Employedto.toentity(empdto);
		Employee empsaved=employeRepository.save(emp);
		Employedto empsaveddto=Employedto.fromentity(empsaved);
		return empsaveddto;
	}

	@Override
	public void deletbyid(Long id) {
		Optional<Employee> optemp =employeRepository.findById(id);
		if (optemp.isPresent())
		{
			employeRepository.deleteById(id);
				
		}
		else
		{
			throw new RuntimeException(" not exist" + id);
				
		}
			
	}

	@Override
	public Employedto metreajour(Employedto empdto) {
		Optional<Employee> optemp =employeRepository.findById(empdto.getId());
		if (optemp.isPresent())
		{
			optemp.get().setFullname(empdto.getFullname());
			optemp.get().setAge(empdto.getAge());
			optemp.get().setDateRecrutement(empdto.getDateRecrutement());
			return Employedto.fromentity(employeRepository.save(optemp.get()));
		}
		else
		{
			throw new RuntimeException(" not exist" + empdto.getId());
				
		}
	}

	@Override
	public Employedto findbyid(Long id) {
		Optional<Employee> optemp =employeRepository.findById(id);
		if (optemp.isPresent())
		{
			return Employedto.fromentity(optemp.get());
				
		}
		else
		{
			throw new RuntimeException(" not exist" + id);
				
		}
	}

	@Override
	public List<Employedto> lister() {
		return employeRepository.findAll().stream().map(Employedto::fromentity).collect(Collectors.toList());
	}

	public List<Employee> listerr() {
		return employeRepository.findAll();
	}
	@Override
	public List<Employedto> chercherparnom(String key) {
		return employeRepository.findemployebyname(key).stream().map(Employedto::fromentity).collect(Collectors.toList());
	}

}
