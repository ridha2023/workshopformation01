package tn.barmegtech.workshopformationspring.servics;

import java.util.List;

import tn.barmegtech.workshopformationspring.dto.Employedto;

public interface EmployeSevice {
	Employedto ajouter(Employedto empdto);
	void deletbyid(Long id);
	Employedto metreajour(Employedto empdto);
	Employedto findbyid(Long id);
	List<Employedto> lister();
	List<Employedto> chercherparnom(String key);

}
