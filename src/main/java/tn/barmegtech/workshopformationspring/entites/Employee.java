package tn.barmegtech.workshopformationspring.entites;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	 @Size(min = 3, max = 20)
	 @NotBlank
	 @Column(name="nomprenom",nullable  =false)
	private String fullname;
	 //@Transient
	private Long age;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateRecrutement;

}
