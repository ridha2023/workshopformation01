package tn.barmegtech.workshopformationspring.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import tn.barmegtech.workshopformationspring.dto.Employedto;
import tn.barmegtech.workshopformationspring.servics.EmployeSevice;

@RestController
@RequestMapping("/api/v1/employe")
@RequiredArgsConstructor
public class Employecontroller {
	private final EmployeSevice employeSevice;
@PostMapping("/saveemp")
	public Employedto ajouter(@RequestBody Employedto empdto) {
		return employeSevice.ajouter(empdto);
	}
@DeleteMapping("/deleteemp/{id}")
	public void deletbyid(@PathVariable("id") Long id) {
		employeSevice.deletbyid(id);
	}
@PostMapping("/updateemp")
	public Employedto metreajour(@RequestBody Employedto empdto) {
		return employeSevice.metreajour(empdto);
	}
	@GetMapping("/findbyid/{id}")
	public Employedto findbyid(@PathVariable("id") Long id) {
		return employeSevice.findbyid(id);
	}
	@GetMapping("/lister")
	public List<Employedto> lister() {
		return employeSevice.lister();
	}
	@GetMapping("/lister/{key}")
	public List<Employedto> chercherparnom(@PathVariable("key") String key) {
		return employeSevice.chercherparnom(key);
	}
	

}
